# sources and download
# http://git.zerfleddert.de/cgi-bin/gitweb.cgi/hmcfgusb
FROM alpine:3.12 AS build
MAINTAINER mbehlau

ARG HMLAND_VERSION=0.103

RUN apk add --no-cache  --virtual .build-deps \
	libusb-dev \
	g++ \
	make \
	curl \
  && curl http://git.zerfleddert.de/hmcfgusb/releases/{hmcfgusb-$HMLAND_VERSION.tar.gz} --output /tmp/'#1' \
  && tar -xzf /tmp/hmcfgusb-$HMLAND_VERSION.tar.gz -C /opt \
  && rm /tmp/hmcfgusb-$HMLAND_VERSION.tar.gz \
  && cd /opt/hmcfgusb-$HMLAND_VERSION \
  && ln -s /opt/hmcfgusb-$HMLAND_VERSION /opt/hmcfgusb \
  && make \
  && rm *.h *.o *.c *.d \
  && apk del .build-deps \
  && apk add --no-cache libusb coreutils
  
EXPOSE 1000

WORKDIR /opt/hmcfgusb

CMD ["/opt/hmcfgusb-0.103/hmland", "-p 1000"]
